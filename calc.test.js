const calc = require('./calc')

test('adds 1 + 2 to equal 3', () => {
  expect(calc.sum(1, 2)).toBe(3)
})

test('minus 2 - 1 to equal 1', () => {
  expect(calc.minus(2, 1)).toBe(1)
})

test('multiply 2 times 2 to equal 4', () => {
  expect(calc.multiply(2, 2)).toBe(4)
})

test('divide 2 / 2 to equal 1', () => {
  expect(calc.divide(2, 2)).toBe(1)
})

test('bad divide 2 / 0 should fail', () => {
  expect(()=>{
    calc.divide(2, 0)
  }).toThrow("Not possible")
})
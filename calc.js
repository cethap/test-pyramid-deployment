const sum = function (a, b) {
    return a + b;
}

const minus = function (a, b) {
    return a - b;
}

const multiply = function (a, b) {
    return a * b;
}

const divide = function (a, b) {
    if(b==0){
        throw "Not possible"
    }
    return a / b;
}

const calc = { sum, minus, multiply, divide }

module.exports = calc
FROM node:14.17-alpine3.10
RUN mkdir /automation
WORKDIR /automation
COPY . .
RUN rm -rf .git
RUN npm install

ENTRYPOINT [ "npm", "test" ]